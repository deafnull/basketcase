﻿using BasketCase.Client;
using BasketCase.Contracts.Models;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace BasketCase.Test.Client
{
    public class BasketApiClientTests : IClassFixture<AuthorizeWebApplicationFactoryFixture>
    {
        private readonly BasketApiClient client;

        public BasketApiClientTests(AuthorizeWebApplicationFactoryFixture fixture)
        {
            this.client = new BasketApiClient(fixture.Factory.CreateClient(), fixture.Token);
        }

        [Fact]
        public async Task Create_RouteReturnsNewBasket()
        {
            Basket result = await client.Create();

            Assert.NotNull(result);
            Assert.NotEqual(default(Guid), result.Id);
            Assert.Empty(result.Items);
        }

        [Fact]
        public async Task Get_ThrowExceptionIfNotExists()
        {
            await Assert.ThrowsAsync<HttpRequestException>(async () => await client.Get(Guid.NewGuid()));
        }

        [Fact]
        public async Task Get_ReturnsBasketIfCreated()
        {
            Basket created = await client.Create();

            Basket returned = await client.Get(created.Id);

            Assert.NotSame(created, returned);
            Assert.Equal(created.Id, returned.Id);
        }

        [Fact]
        public async Task Put_ReturnBasketWithAddedItems()
        {
            BasketItem item = new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() };
            Basket input = new Basket()
            {
                Items = new[] { item }
            };
            Basket result = await client.Put(input);

            Assert.NotNull(result);
            Assert.Equal(input.Id, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.NotSame(input, result);
            Assert.NotEqual(input.Items, result.Items);
            Assert.Contains(result.Items,
                i => i.ProductId == item.ProductId && i.Quantity == item.Quantity);
        }

        [Fact]
        public async Task Put_ReturnPreviouslyCreatedBasketWithAddedItems()
        {
            Basket input = await client.Create();
            BasketItem item = new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() };
            input.Items = new[] { item };
            Basket result = await client.Put(input);

            Assert.NotNull(result);
            Assert.Equal(input.Id, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.NotSame(input, result);
            Assert.NotEqual(input.Items, result.Items);
            Assert.Contains(result.Items,
                i => i.ProductId == item.ProductId && i.Quantity == item.Quantity);
        }

        [Fact]
        public async Task Put_ReturnAggregatedBasketDifferentProduct()
        {
            BasketItem validItem1 = new BasketItem() { Quantity = 1, ProductId = Guid.NewGuid() };
            BasketItem validItem2 = new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() };
            BasketItem invalidItem = new BasketItem() { Quantity = 0, ProductId = Guid.NewGuid() };
            Guid basketId = Guid.NewGuid();

            await client.Put(new Basket(basketId)
            {
                Items = new[] { validItem1 }
            });
            Basket result = await client.Put(new Basket(basketId)
            {
                Items = new[] { validItem2, invalidItem }
            });
            
            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.True(result.Items.Count() == 2);
            Assert.Contains(result.Items,
                i => i.ProductId == validItem1.ProductId && i.Quantity == validItem1.Quantity);
            Assert.Contains(result.Items,
                i => i.ProductId == validItem1.ProductId && i.Quantity == validItem1.Quantity);
            Assert.DoesNotContain(result.Items, i => i.ProductId == invalidItem.ProductId);
        }

        [Fact]
        public async Task Put_ReturnAggregatedBasketSameProduct()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            await client.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            await client.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            Basket result = await client.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = -3 } }
            });

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.True(result.Items.Count() == 1);
            Assert.Contains(result.Items,
                i => i.ProductId == productId && i.Quantity == 2);
        }

        [Fact]
        public async Task Get_ReturnsPreviouslyPutBasket()
        {
            Basket input = new Basket()
            {
                Items = new[] { new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() } }
            };
            Basket put = await client.Put(input);

            Basket get = await client.Get(input.Id);

            Assert.NotSame(put, get);
            Assert.Equal(put.Id, get.Id);
            Assert.True(get.Items.Count() == 1);
        }

        [Fact]
        public async Task Override_SameProductOverridesQuantity()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            await client.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 4 } }
            });
            await client.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });

            Basket result = await client.Get(basketId);

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.True(result.Items.Count() == 1);
            Assert.Contains(result.Items,
                i => i.ProductId == productId && i.Quantity == 1);
        }

        [Fact]
        public async Task Override_SameProductWithQuantityLessThanOneRemovesItem()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            await client.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 4 } }
            });
            await client.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = -3 } }
            });

            Basket result = await client.Get(basketId);

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.Empty(result.Items);
        }

        [Fact]
        public async Task Override_PreviousItemsAreRemoved()
        {
            Guid expectedProductId = Guid.NewGuid();
            Guid unexpectedProductId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            await client.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = unexpectedProductId, Quantity = 4 } }
            });
            await client.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = expectedProductId, Quantity = 4 } }
            });

            Basket result = await client.Get(basketId);

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.True(result.Items.Count() == 1);
            Assert.Contains(result.Items,
                i => i.ProductId == expectedProductId);
            Assert.DoesNotContain(result.Items,
                i => i.ProductId == unexpectedProductId);
        }

        [Fact]
        public async Task RemoveItems_ExistingItem()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            await client.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            Basket result = await client.RemoveItems(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId } }
            });

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.Empty(result.Items);
        }

        [Fact]
        public async Task RemoveItems_NonexistingItem()
        {
            Guid expectedProductId = Guid.NewGuid();
            Guid unexpectedProductId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            await client.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = expectedProductId, Quantity = 1 } }
            });
            Basket result = await client.RemoveItems(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = unexpectedProductId } }
            });

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.Contains(result.Items,
                i => i.ProductId == expectedProductId);
            Assert.DoesNotContain(result.Items,
                i => i.ProductId == unexpectedProductId);
        }

        [Fact]
        public async Task Clear_BasketItemsAreClearedButBasketStillExists()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            await client.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            await client.Clear(basketId);

            Basket result = await client.Get(basketId);

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.Empty(result.Items);
        }

        [Fact]
        public async Task Clear_NonexistingBasketWillNotBeAddedAfterClear()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            await client.Clear(basketId);

            await Assert.ThrowsAsync<HttpRequestException>(async () => await client.Get(basketId));
        }
    }
}
