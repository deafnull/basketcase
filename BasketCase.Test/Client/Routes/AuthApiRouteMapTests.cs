﻿using BasketCase.Api;
using BasketCase.Client.Routes;
using BasketCase.Contracts;
using BasketCase.Contracts.Models;
using BasketCase.Core;
using BasketCase.Test.Core;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace BasketCase.Test.Client.Routes
{
    public class AuthApiRouteMapTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient alwaysValidClient;
        private readonly HttpClient neverValidClient;

        public AuthApiRouteMapTests(WebApplicationFactory<Startup> fixture)
        {
            alwaysValidClient = fixture.WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    services.AddTransient<IAuthService, AlwaysValidApiKeyAuthService>();
                });
            }).CreateClient();

            neverValidClient = fixture.WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    services.AddTransient<IAuthService, NeverValidApiKeyAuthService>();
                });
            }).CreateClient();
        }

        [Fact]
        public async Task HttpPost_AuthRequestToken_ValidApiKeyReturnsSuccessResponse()
        {
            AuthApiRouteMap router = new AuthApiRouteMap(alwaysValidClient);
            TokenRequest request = new TokenRequest() { ApiKey = "test" };

            HttpResponseMessage response = await router.RequestToken(request);

            response.EnsureSuccessStatusCode();
            Assert.Equal("utf-8", response.Content.Headers.ContentType.CharSet);
            Assert.Equal("application/json", response.Content.Headers.ContentType.MediaType);
            Assert.Equal(HttpMethod.Post, response.RequestMessage.Method);
        }

        [Fact]
        public async Task HttpPost_AuthRequestToken_InvalidApiKey()
        {
            AuthApiRouteMap router = new AuthApiRouteMap(neverValidClient);
            TokenRequest request = new TokenRequest() { ApiKey = "test" };

            HttpResponseMessage response = await router.RequestToken(request);

            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
            Assert.False(response.IsSuccessStatusCode);
            Assert.Equal("utf-8", response.Content.Headers.ContentType.CharSet);
            Assert.Equal("application/json", response.Content.Headers.ContentType.MediaType);
            Assert.Equal(HttpMethod.Post, response.RequestMessage.Method);
        }
    }
}
