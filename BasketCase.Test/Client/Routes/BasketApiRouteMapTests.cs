﻿using BasketCase.Client.Routes;
using BasketCase.Contracts.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace BasketCase.Test.Client.Routes
{
    public class BasketApiRouteMapTests : IClassFixture<AuthorizeWebApplicationFactoryFixture>
    {
        private readonly BasketApiRouteMap router;

        public BasketApiRouteMapTests(AuthorizeWebApplicationFactoryFixture fixture)
        {
            HttpClient client = fixture.Factory.CreateClient();
            router = new BasketApiRouteMap(client, fixture.Token);
        }

        [Fact]
        public async Task HttpGet_BasketCreate_RouteReturnsSuccessResponse()
        {
            HttpResponseMessage response = await router.Create();

            response.EnsureSuccessStatusCode();
            Assert.Equal("utf-8", response.Content.Headers.ContentType.CharSet);
            Assert.Equal("application/json", response.Content.Headers.ContentType.MediaType);
            Assert.Equal(HttpMethod.Get, response.RequestMessage.Method);
        }

        [Fact]
        public async Task HttpGet_BasketGet_NotFoundIfNotExists()
        {
            HttpResponseMessage response = await router.Get(Guid.NewGuid());

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
            Assert.Equal(HttpMethod.Get, response.RequestMessage.Method);
        }

        [Fact]
        public async Task HttpPut_BasketPut_RouteReturnsSuccessResponse()
        {
            HttpResponseMessage response = await router.Put(new Basket(Guid.NewGuid())
            {
                Items = new[] { new BasketItem() { ProductId = Guid.NewGuid(), Quantity = 1 } }
            });

            response.EnsureSuccessStatusCode();
            Assert.Equal("utf-8", response.Content.Headers.ContentType.CharSet);
            Assert.Equal("application/json", response.Content.Headers.ContentType.MediaType);
            Assert.Equal(HttpMethod.Put, response.RequestMessage.Method);
        }

        [Fact]
        public async Task HttpPost_BasketOverride_RouteReturnsSuccessResponse()
        {
            HttpResponseMessage response = await router.Override(new Basket(Guid.NewGuid())
            {
                Items = new[] { new BasketItem() { ProductId = Guid.NewGuid(), Quantity = 1 } }
            });

            response.EnsureSuccessStatusCode();

            Assert.Equal(HttpMethod.Post, response.RequestMessage.Method);
        }

        [Fact]
        public async Task HttpDelete_BasketClear_RouteReturnsSuccessResponse()
        {
            HttpResponseMessage response = await router.Clear(Guid.NewGuid());

            response.EnsureSuccessStatusCode();

            Assert.Equal(HttpMethod.Delete, response.RequestMessage.Method);
        }

        [Fact]
        public async Task HttpPut_BasketRemoveItems_RouteReturnsSuccessResponse()
        {
            HttpResponseMessage response = await router.RemoveItems(new Basket(Guid.NewGuid())
            {
                Items = new[] { new BasketItem() { ProductId = Guid.NewGuid(), Quantity = 1 } }
            });

            response.EnsureSuccessStatusCode();
            Assert.Equal("utf-8", response.Content.Headers.ContentType.CharSet);
            Assert.Equal("application/json", response.Content.Headers.ContentType.MediaType);
            Assert.Equal(HttpMethod.Put, response.RequestMessage.Method);
        }
    }
}
