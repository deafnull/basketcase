﻿using BasketCase.Api;
using BasketCase.Client;
using BasketCase.Contracts.Models;
using BasketCase.Core;
using BasketCase.Test.Core;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace BasketCase.Test.Client
{
    public class AuthApiClientTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private readonly HttpClient alwaysValidClient;
        private readonly HttpClient neverValidClient;

        public AuthApiClientTests(WebApplicationFactory<Startup> fixture)
        {
            alwaysValidClient = fixture.WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    services.AddTransient<IAuthService, AlwaysValidApiKeyAuthService>();
                });
            }).CreateClient();

            neverValidClient = fixture.WithWebHostBuilder(builder =>
            {
                builder.ConfigureTestServices(services =>
                {
                    services.AddTransient<IAuthService, NeverValidApiKeyAuthService>();
                });
            }).CreateClient();
        }

        [Fact]
        public async Task RequestToken_ValidApiKey()
        {
            AuthApiClient client = new AuthApiClient(alwaysValidClient);

            TokenResponse result = await client.RequestToken("test");

            Assert.NotNull(result);
            Assert.NotNull(result.Token);
        }

        [Fact]
        public async Task RequestToken_InvalidApiKeyThrowsException()
        {
            AuthApiClient client = new AuthApiClient(neverValidClient);

            await Assert.ThrowsAsync<HttpRequestException>(async () => await client.RequestToken("test"));
        }
    }
}
