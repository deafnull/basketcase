﻿using BasketCase.Contracts.Models;
using BasketCase.Core;
using System;
using System.Collections.Concurrent;

namespace BasketCase.Test.Core
{
    internal class MStorageProvider : IStorageProvider<Guid, Basket>
    {
        private ConcurrentDictionary<Guid, Basket> dictionary = new ConcurrentDictionary<Guid, Basket>();

        public Basket Get(Guid key)
        {
            Basket val = default(Basket);
            dictionary.TryGetValue(key, out val);
            return val;
        }

        public void Store(Guid key, Basket value)
        {
            dictionary[key] = value;
        }

        public void Remove(Guid key)
        {
            Basket val = default(Basket);
            dictionary.TryRemove(key, out val);
        }
    }

    internal class AlwaysValidApiKeyAuthService : IAuthService
    {
        public bool ValidateKey(string apiKey)
        {
            return true;
        }
    }

    internal class NeverValidApiKeyAuthService : IAuthService
    {
        public bool ValidateKey(string apiKey)
        {
            return false;
        }
    }
}