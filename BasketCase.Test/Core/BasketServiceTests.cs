﻿using BasketCase.Contracts.Models;
using BasketCase.Core;
using System;
using System.Linq;
using Xunit;

namespace BasketCase.Test.Core
{
    public class BasketServiceTest
    {
        private readonly IBasketService basketService = new BasketService(new MStorageProvider());

        [Fact]
        public void Create_ReturnsNewBasket()
        {
            Basket result = basketService.Create();

            Assert.NotNull(result);
            Assert.NotEqual(default(Guid), result.Id);
            Assert.Empty(result.Items);
        }

        [Fact]
        public void Get_ReturnNullIfNotExists()
        {
            Basket result = basketService.Get(Guid.NewGuid());

            Assert.Null(result);
        }

        [Fact]
        public void Get_ReturnsBasketIfCreated()
        {
            Basket created = basketService.Create();

            Basket returned = basketService.Get(created.Id);

            Assert.Same(created, returned);
            Assert.Equal(created.Id, returned.Id);
        }

        [Fact]
        public void Put_ReturnBasketWithAddedItems()
        {
            BasketItem item = new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() };
            Basket input = new Basket()
            {
                Items = new[] { item }
            };
            Basket result = basketService.Put(input);

            Assert.NotNull(result);
            Assert.Equal(input.Id, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.NotSame(input, result);
            Assert.Equal(input.Items, result.Items);
            Assert.Contains(result.Items,
                i => i.ProductId == item.ProductId && i.Quantity == item.Quantity);
        }

        [Fact]
        public void Put_ReturnPreviouslyCreatedBasketWithAddedItems()
        {
            Basket input = basketService.Create();
            BasketItem item = new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() };
            input.Items = new[] { item };
            Basket result = basketService.Put(input);

            Assert.NotNull(result);
            Assert.Equal(input.Id, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.NotSame(input, result);
            Assert.Equal(input.Items, result.Items);
            Assert.Contains(result.Items,
                i => i.ProductId == item.ProductId && i.Quantity == item.Quantity);
        }

        [Fact]
        public void Put_ReturnAggregatedBasketDifferentProduct()
        {
            BasketItem validItem1 = new BasketItem() { Quantity = 1, ProductId = Guid.NewGuid() };
            BasketItem validItem2 = new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() };
            BasketItem invalidItem = new BasketItem() { Quantity = 0, ProductId = Guid.NewGuid() };
            Guid basketId = Guid.NewGuid();

            basketService.Put(new Basket(basketId)
            {
                Items = new[] { validItem1 }
            });
            Basket result = basketService.Put(new Basket(basketId)
            {
                Items = new[] { validItem2, invalidItem }
            });

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.True(result.Items.Count() == 2);
            Assert.Contains(result.Items,
                i => i.ProductId == validItem1.ProductId && i.Quantity == validItem1.Quantity);
            Assert.Contains(result.Items,
                i => i.ProductId == validItem1.ProductId && i.Quantity == validItem1.Quantity);
            Assert.DoesNotContain(result.Items, i => i.ProductId == invalidItem.ProductId);
        }

        [Fact]
        public void Put_ReturnAggregatedBasketSameProduct()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            basketService.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            basketService.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            Basket result = basketService.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = -3 } }
            });

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.True(result.Items.Count() == 1);
            Assert.Contains(result.Items,
                i => i.ProductId == productId && i.Quantity == 2);
        }

        [Fact]
        public void Get_ReturnsPreviouslyPutBasket()
        {
            Basket input = new Basket()
            {
                Items = new[] { new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() } }
            };
            Basket put = basketService.Put(input);

            Basket get = basketService.Get(input.Id);

            Assert.Same(put, get);
            Assert.Equal(put.Id, get.Id);
            Assert.True(get.Items.Count() == 1);
        }

        [Fact]
        public void Override_SameProductOverridesQuantity()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            basketService.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 4 } }
            });
            basketService.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });

            Basket result = basketService.Get(basketId);

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.True(result.Items.Count() == 1);
            Assert.Contains(result.Items,
                i => i.ProductId == productId && i.Quantity == 1);
        }

        [Fact]
        public void Override_SameProductWithQuantityLessThanOneRemovesItem()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            basketService.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 4 } }
            });
            basketService.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = -3 } }
            });

            Basket result = basketService.Get(basketId);

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.Empty(result.Items);
        }

        [Fact]
        public void Override_PreviousItemsAreRemoved()
        {
            Guid expectedProductId = Guid.NewGuid();
            Guid unexpectedProductId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            basketService.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = unexpectedProductId, Quantity = 4 } }
            });
            basketService.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = expectedProductId, Quantity = 4 } }
            });

            Basket result = basketService.Get(basketId);

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.True(result.Items.Count() == 1);
            Assert.Contains(result.Items,
                i => i.ProductId == expectedProductId);
            Assert.DoesNotContain(result.Items,
                i => i.ProductId == unexpectedProductId);
        }

        [Fact]
        public void RemoveItems_ExistingItem()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            basketService.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            Basket result = basketService.RemoveItems(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId } }
            });

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.Empty(result.Items);
        }

        [Fact]
        public void RemoveItems_NonexistingItem()
        {
            Guid expectedProductId = Guid.NewGuid();
            Guid unexpectedProductId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            basketService.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = expectedProductId, Quantity = 1 } }
            });
            Basket result = basketService.RemoveItems(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = unexpectedProductId } }
            });

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.NotEmpty(result.Items);
            Assert.Contains(result.Items,
                i => i.ProductId == expectedProductId);
            Assert.DoesNotContain(result.Items,
                i => i.ProductId == unexpectedProductId);
        }

        [Fact]
        public void Clear_BasketItemsAreClearedButBasketStillExists()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            basketService.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            basketService.Clear(basketId);

            Basket result = basketService.Get(basketId);

            Assert.NotNull(result);
            Assert.Equal(basketId, result.Id);
            Assert.Empty(result.Items);
        }

        [Fact]
        public void Clear_NonexistingBasketWillNotBeAddedAfterClear()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            basketService.Clear(basketId);

            Basket result = basketService.Get(Guid.NewGuid());

            Assert.Null(result);
        }

    }
}
