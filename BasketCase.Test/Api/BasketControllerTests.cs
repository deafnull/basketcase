﻿using BasketCase.Api.Controllers;
using BasketCase.Contracts.Models;
using BasketCase.Core;
using BasketCase.Test.Core;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using Xunit;

namespace BasketCase.Test.Api
{
    public class BasketControllerTests
    {
        private readonly BasketController controller;

        public BasketControllerTests()
        {
            IBasketService basketService = new BasketService(new MStorageProvider());
            controller = new BasketController(basketService);
        }

        [Fact]
        public void Create_ReturnsNewBasket()
        {
            ActionResult<Basket> result = controller.Create();

            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.NotEqual(default(Guid), result.Value.Id);
            Assert.Empty(result.Value.Items);
        }

        [Fact]
        public void Get_ReturnNullIfNotExists()
        {
            ActionResult<Basket> result = controller.Get(Guid.NewGuid());

            Assert.Null(result.Value);
        }

        [Fact]
        public void Get_ReturnsBasketIfCreated()
        {
            ActionResult<Basket> created = controller.Create();

            ActionResult<Basket> returned = controller.Get(created.Value.Id);

            Assert.Same(created.Value, returned.Value);
            Assert.Equal(created.Value.Id, returned.Value.Id);
        }

        [Fact]
        public void Put_ReturnBasketWithAddedItems()
        {
            BasketItem item = new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() };
            Basket input = new Basket()
            {
                Items = new[] { item }
            };
            ActionResult<Basket> result = controller.Put(input);

            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.Equal(input.Id, result.Value.Id);
            Assert.NotEmpty(result.Value.Items);
            Assert.NotSame(input, result);
            Assert.Equal(input.Items, result.Value.Items);
            Assert.Contains(result.Value.Items,
                i => i.ProductId == item.ProductId && i.Quantity == item.Quantity);
        }

        [Fact]
        public void Put_ReturnPreviouslyCreatedBasketWithAddedItems()
        {
            ActionResult<Basket> input = controller.Create();
            BasketItem item = new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() };
            input.Value.Items = new[] { item };
            ActionResult<Basket> result = controller.Put(input.Value);

            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.Equal(input.Value.Id, result.Value.Id);
            Assert.NotEmpty(result.Value.Items);
            Assert.NotSame(input, result);
            Assert.Equal(input.Value.Items, result.Value.Items);
            Assert.Contains(result.Value.Items,
                i => i.ProductId == item.ProductId && i.Quantity == item.Quantity);
        }

        [Fact]
        public void Put_ReturnAggregatedBasketDifferentProduct()
        {
            BasketItem validItem1 = new BasketItem() { Quantity = 1, ProductId = Guid.NewGuid() };
            BasketItem validItem2 = new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() };
            BasketItem invalidItem = new BasketItem() { Quantity = 0, ProductId = Guid.NewGuid() };
            Guid basketId = Guid.NewGuid();

            controller.Put(new Basket(basketId)
            {
                Items = new[] { validItem1 }
            });
            ActionResult<Basket> result = controller.Put(new Basket(basketId)
            {
                Items = new[] { validItem2, invalidItem }
            });

            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.Equal(basketId, result.Value.Id);
            Assert.NotEmpty(result.Value.Items);
            Assert.True(result.Value.Items.Count() == 2);
            Assert.Contains(result.Value.Items,
                i => i.ProductId == validItem1.ProductId && i.Quantity == validItem1.Quantity);
            Assert.Contains(result.Value.Items,
                i => i.ProductId == validItem1.ProductId && i.Quantity == validItem1.Quantity);
            Assert.DoesNotContain(result.Value.Items, i => i.ProductId == invalidItem.ProductId);
        }

        [Fact]
        public void Put_ReturnAggregatedBasketSameProduct()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            controller.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            controller.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            ActionResult<Basket> result = controller.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = -3 } }
            });

            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.Equal(basketId, result.Value.Id);
            Assert.NotEmpty(result.Value.Items);
            Assert.True(result.Value.Items.Count() == 1);
            Assert.Contains(result.Value.Items,
                i => i.ProductId == productId && i.Quantity == 2);
        }

        [Fact]
        public void Get_ReturnsPreviouslyPutBasket()
        {
            Basket input = new Basket()
            {
                Items = new[] { new BasketItem() { Quantity = 4, ProductId = Guid.NewGuid() } }
            };
            ActionResult<Basket> put = controller.Put(input);

            ActionResult<Basket> get = controller.Get(input.Id);

            Assert.Same(put.Value, get.Value);
            Assert.Equal(put.Value.Id, get.Value.Id);
            Assert.True(get.Value.Items.Count() == 1);
        }

        [Fact]
        public void Override_SameProductOverridesQuantity()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            controller.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 4 } }
            });
            controller.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });

            ActionResult<Basket> result = controller.Get(basketId);

            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.Equal(basketId, result.Value.Id);
            Assert.NotEmpty(result.Value.Items);
            Assert.True(result.Value.Items.Count() == 1);
            Assert.Contains(result.Value.Items,
                i => i.ProductId == productId && i.Quantity == 1);
        }

        [Fact]
        public void Override_SameProductWithQuantityLessThanOneRemovesItem()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            controller.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 4 } }
            });
            controller.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = -3 } }
            });

            ActionResult<Basket> result = controller.Get(basketId);

            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.Equal(basketId, result.Value.Id);
            Assert.Empty(result.Value.Items);
        }

        [Fact]
        public void Override_PreviousItemsAreRemoved()
        {
            Guid expectedProductId = Guid.NewGuid();
            Guid unexpectedProductId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            controller.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = unexpectedProductId, Quantity = 4 } }
            });
            controller.Override(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = expectedProductId, Quantity = 4 } }
            });

            ActionResult<Basket> result = controller.Get(basketId);

            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.Equal(basketId, result.Value.Id);
            Assert.NotEmpty(result.Value.Items);
            Assert.True(result.Value.Items.Count() == 1);
            Assert.Contains(result.Value.Items,
                i => i.ProductId == expectedProductId);
            Assert.DoesNotContain(result.Value.Items,
                i => i.ProductId == unexpectedProductId);
        }

        [Fact]
        public void RemoveItems_ExistingItem()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            controller.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            ActionResult<Basket> result = controller.RemoveItems(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId } }
            });

            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.Equal(basketId, result.Value.Id);
            Assert.Empty(result.Value.Items);
        }

        [Fact]
        public void RemoveItems_NonexistingItem()
        {
            Guid expectedProductId = Guid.NewGuid();
            Guid unexpectedProductId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            controller.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = expectedProductId, Quantity = 1 } }
            });
            ActionResult<Basket> result = controller.RemoveItems(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = unexpectedProductId } }
            });

            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.Equal(basketId, result.Value.Id);
            Assert.NotEmpty(result.Value.Items);
            Assert.Contains(result.Value.Items,
                i => i.ProductId == expectedProductId);
            Assert.DoesNotContain(result.Value.Items,
                i => i.ProductId == unexpectedProductId);
        }

        [Fact]
        public void Clear_BasketItemsAreClearedButBasketStillExists()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            controller.Put(new Basket(basketId)
            {
                Items = new[] { new BasketItem() { ProductId = productId, Quantity = 1 } }
            });
            controller.Clear(basketId);

            ActionResult<Basket> result = controller.Get(basketId);

            Assert.NotNull(result);
            Assert.NotNull(result.Value);
            Assert.Equal(basketId, result.Value.Id);
            Assert.Empty(result.Value.Items);
        }

        [Fact]
        public void Clear_NonexistingBasketWillNotBeAddedAfterClear()
        {
            Guid productId = Guid.NewGuid();
            Guid basketId = Guid.NewGuid();

            controller.Clear(basketId);

            ActionResult<Basket> result = controller.Get(Guid.NewGuid());

            Assert.Null(result.Value);
        }
    }
}
