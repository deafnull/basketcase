﻿using BasketCase.Api.Controllers;
using BasketCase.Contracts.Models;
using BasketCase.Core;
using BasketCase.Test.Core;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.IO;
using Xunit;

namespace BasketCase.Test.Api
{
    public class AuthControllerTests
    {
        private readonly IConfigurationRoot configuration;

        public AuthControllerTests()
        {
            var builder = new ConfigurationBuilder();
            configuration = builder.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true).Build();
            configuration[Consts.Config.JwtSecretKey] = "d41d8cd98f00b204e9800998ecf8427e";
            configuration[Consts.Config.JwtIssuer] = "BasketCase";
            configuration[Consts.Config.JwtAudience] = "BasketCase";
        }

        [Fact]
        public void RequestToken_ValidApiKey()
        {
            IAuthService authService = new AlwaysValidApiKeyAuthService();
            AuthController controller = new AuthController(configuration, authService);
            TokenRequest request = new TokenRequest() { ApiKey = "bfa2322d41bb1ce3624b3eeed29512f6" };

            ActionResult<TokenResponse> result = controller.RequestToken(request);
            OkObjectResult okResult = result.Result as OkObjectResult;

            Assert.IsAssignableFrom<OkObjectResult>(result.Result);
            Assert.NotNull(okResult.Value);
            Assert.NotNull(((TokenResponse)okResult.Value).Token);
        }

        [Fact]
        public void RequestToken_InvalidApiKey()
        {
            IAuthService authService = new NeverValidApiKeyAuthService();
            AuthController controller = new AuthController(configuration, authService);
            TokenRequest request = new TokenRequest() { ApiKey = "bfa2322d41bb1ce3624b3eeed29512f6" };

            ActionResult<TokenResponse> result = controller.RequestToken(request);
            BadRequestObjectResult badRequestResult = result.Result as BadRequestObjectResult;

            Assert.IsAssignableFrom<BadRequestObjectResult>(result.Result);
            Assert.NotNull(badRequestResult.Value);
            Assert.Contains("Invalid API key.", badRequestResult.Value.ToString());
        }
    }
}
