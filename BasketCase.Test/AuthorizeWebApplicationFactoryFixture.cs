﻿using BasketCase.Api;
using BasketCase.Client;
using BasketCase.Contracts.Models;
using Microsoft.AspNetCore.Mvc.Testing;
using System;
using System.Threading.Tasks;

namespace BasketCase.Test
{
    public class AuthorizeWebApplicationFactoryFixture : IDisposable
    {
        public AuthorizeWebApplicationFactoryFixture()
        {
            AuthApiClient apiClient = new AuthApiClient(Factory.CreateClient());
            Task<TokenResponse> t = apiClient.RequestToken("test");
            t.Wait();
            Token = t.Result.Token;
        }

        public WebApplicationFactory<Startup> Factory { get; } = new WebApplicationFactory<Startup>();
        public string Token { get; }

        public void Dispose()
        {
            Factory.Dispose();
        }
    }
}