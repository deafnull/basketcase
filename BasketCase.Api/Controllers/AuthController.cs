﻿using BasketCase.Contracts;
using BasketCase.Contracts.Models;
using BasketCase.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace BasketCase.Api.Controllers
{
    [Route("[controller]/[action]")]
    [Produces("application/json")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IConfiguration config;
        private readonly IAuthService authService;

        public AuthController(IConfiguration config, IAuthService authService)
        {
            this.config = config;
            this.authService = authService;
        }

        [AllowAnonymous]
        [HttpPost(Name = nameof(IAuthApi.RequestToken))]
        public ActionResult<TokenResponse> RequestToken([FromBody] TokenRequest request)
        {
            if (authService.ValidateKey(request.ApiKey))
            {
                Claim[] claims = new[]
                {
                    new Claim(Consts.ClaimTypes.ApiKey, request.ApiKey)
                };

                byte[] keyByteArray = Encoding.UTF8.GetBytes(config[Consts.Config.JwtSecretKey]);
                SymmetricSecurityKey ssKey = new SymmetricSecurityKey(keyByteArray);
                SigningCredentials creds = new SigningCredentials(ssKey, SecurityAlgorithms.HmacSha256);

                JwtSecurityToken token = new JwtSecurityToken(
                    issuer: config[Consts.Config.JwtIssuer],
                    audience: config[Consts.Config.JwtAudience],
                    claims: claims,
                    expires: DateTime.Now.AddMinutes(30),
                    signingCredentials: creds);

                return Ok(new TokenResponse()
                {
                    Token = new JwtSecurityTokenHandler().WriteToken(token)
                });
            }

            return BadRequest("Invalid API key.");
        }
    }
}
