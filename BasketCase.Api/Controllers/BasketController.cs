﻿using BasketCase.Contracts;
using BasketCase.Contracts.Models;
using BasketCase.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;

namespace BasketCase.Api.Controllers
{
    [Route("[controller]/[action]/{id?}")]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class BasketController : ControllerBase
    {
        private readonly IBasketService basketService;

        public BasketController(IBasketService basketService)
        {
            this.basketService = basketService;
        }

        [HttpGet(Name = nameof(IBasketApi.Create))]
        public ActionResult<Basket> Create()
        {
            return basketService.Create();
        }

        [HttpGet(Name = nameof(IBasketApi.Get))]
        public ActionResult<Basket> Get(Guid id)
        {
            Basket basket = basketService.Get(id);

            if (basket == null)
                return NotFound();

            return basket;
        }

        [HttpPut(Name = nameof(IBasketApi.Put))]
        public ActionResult<Basket> Put([FromBody]Basket basket)
        {
            return basketService.Put(basket);
        }

        [HttpPost(Name = nameof(IBasketApi.Override))]
        public void Override([FromBody]Basket basket)
        {
            basketService.Override(basket);
        }

        [HttpPut(Name = nameof(IBasketApi.RemoveItems))]
        public ActionResult<Basket> RemoveItems([FromBody]Basket basket)
        {
            return basketService.RemoveItems(basket);
        }

        [HttpDelete(Name = nameof(IBasketApi.Clear))]
        public void Clear(Guid id)
        {
            basketService.Clear(id);
        }
    }
}
