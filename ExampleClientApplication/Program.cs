﻿using BasketCase.Client;
using BasketCase.Contracts;
using BasketCase.Contracts.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace ExampleClientApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {
            try
            {
                Uri uri = new Uri("http://localhost:5000");
                using (IAuthApi authApi = new AuthApiClient(uri))
                {
                    TokenResponse response = await authApi.RequestToken("your api key");

                    Console.WriteLine($"Received authorization token: {response.Token}");

                    using (IBasketApi basketApi = new BasketApiClient(uri, response.Token))
                    {
                        Basket basket = await basketApi.Create(); //register new server-side basket
                        Console.WriteLine($"Registered new Basket. Id:{basket.Id}");

                        Guid productId = Guid.NewGuid(); //TODO: get product ids from server
                        basket.Items = new[] { new BasketItem() { ProductId = productId, Quantity = 4 } }; // add items to basket
                        Console.WriteLine($"Added product to Basket. ProductId:{productId}");

                        basket = await basketApi.Put(basket); //put items in server-side basket

                        Console.WriteLine($"Basket items count:{basket.Items.Count()}");
                        Console.WriteLine("Listing items in basket...");
                        Console.WriteLine("ProductId:\tQuantity:");
                        foreach (BasketItem item in basket.Items)
                        {
                            Console.WriteLine($"{item.ProductId}\t{item.Quantity}");
                        }

                        Console.WriteLine("Clearing basket...");
                        await basketApi.Clear(basket.Id); // clear server-side basket

                        Console.WriteLine("Retrieving empty basket...");
                        basket = await basketApi.Get(basket.Id);

                        Console.WriteLine($"Basket items count:{basket.Items.Count()}");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occurred: \n" + ex);
            }
            finally
            {
                Console.WriteLine("Press any key to quit...");
                Console.ReadKey();
            }
        }
    }
}
