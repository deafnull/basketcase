﻿namespace BasketCase.Contracts
{
    public struct ControllerNames
    {
        public const string Auth = "Auth";
        public const string Basket = "Basket";
    }
}
