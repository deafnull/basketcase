﻿using BasketCase.Contracts.Models;
using System;
using System.Threading.Tasks;

namespace BasketCase.Contracts
{
    /// <summary>
    /// Auth API interface
    /// </summary>
    public interface IAuthApi : IDisposable
    {
        /// <summary>
        /// Requests JWT token for given API key
        /// </summary>
        /// <param name="apiKey"></param>
        /// <returns></returns>
        Task<TokenResponse> RequestToken(string apiKey); 
    }
}
