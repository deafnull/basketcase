﻿using BasketCase.Contracts.Models;
using System;
using System.Threading.Tasks;

namespace BasketCase.Contracts
{
    /// <summary>
    /// Basket API interface. Authorization is required.
    /// </summary>
    public interface IBasketApi : IDisposable
    {
        /// <summary>
        /// Creates a new server-side basket.
        /// </summary>
        /// <returns>new Basket</returns>
        Task<Basket> Create();

        /// <summary>
        /// Returns the Basket with items.
        /// If the Basket does not exist on server, an exception is thrown.
        /// </summary>
        /// <param name="basketId"></param>
        /// <returns>Basket</returns>
        Task<Basket> Get(Guid basketId);

        /// <summary>
        /// Puts items into the Basket and returns it with aggregated items.
        /// If the Basket does not exist on server, it is created and stored.
        /// </summary>
        /// <param name="basket">Basket with items to add</param>
        Task<Basket> Put(Basket basket);

        /// <summary>
        /// Overrides items in the Basket.
        /// If the Basket does not exist on server, it is created and stored.
        /// </summary>
        /// <param name="basket"></param>
        Task Override(Basket basket);

        /// <summary>
        /// Removes items from the Basket.
        /// </summary>
        /// <param name="basket">Basket with items to remove</param>
        /// <returns>Basket with items removed</returns>
        Task<Basket> RemoveItems(Basket basket);

        /// <summary>
        /// Removes all items from the server-side Basket.
        /// </summary>
        /// <param name="basketId"></param>
        Task Clear(Guid basketId);
    }
}
