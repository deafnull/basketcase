﻿using System;

namespace BasketCase.Contracts.Models
{
    public class BasketItem
    {
        public decimal Quantity { get; set; }
        public Guid ProductId { get; set; }
    }
}