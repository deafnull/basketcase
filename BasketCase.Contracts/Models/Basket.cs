﻿using System;
using System.Collections.Generic;

namespace BasketCase.Contracts.Models
{
    public class Basket
    {
        public Basket(Guid id = default(Guid))
        {
            if (id == default(Guid))
                Id = Guid.NewGuid();
            else
                Id = id;
        }

        public Guid Id { get; }
        public IEnumerable<BasketItem> Items { get; set; } = new List<BasketItem>();
    }
}