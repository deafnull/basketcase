﻿namespace BasketCase.Core
{
    public interface IAuthService
    {
        bool ValidateKey(string apiKey);
    }

    public class DevelopmentAuthService : IAuthService
    {
        //not intended for production
        //todo implement proper validation

        public bool ValidateKey(string apiKey)
        {
            return !string.IsNullOrWhiteSpace(apiKey);
        }
    }
}
