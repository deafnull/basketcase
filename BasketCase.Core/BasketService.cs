﻿using BasketCase.Contracts.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BasketCase.Core
{
    public interface IBasketService
    {
        Basket Create();
        Basket Get(Guid basketId);
        Basket Put(Basket basket);
        Basket RemoveItems(Basket basket);
        void Override(Basket basket);
        void Clear(Guid basketId);
    }

    public class BasketService : IBasketService
    {
        private readonly IStorageProvider<Guid, Basket> storageProvider;

        public BasketService(IStorageProvider<Guid, Basket> storageProvider)
        {
            this.storageProvider = storageProvider;
        }

        public Basket Create()
        {
            Basket basket = new Basket();
            storageProvider.Store(basket.Id, basket);
            return basket;
        }

        public Basket Get(Guid id)
        {
            return storageProvider.Get(id);
        }

        public Basket Put(Basket input)
        {
            Basket existing = storageProvider.Get(input.Id);
            List<BasketItem> combinedItems = new List<BasketItem>();

            if (existing != null)
                combinedItems.AddRange(existing.Items);

            foreach (BasketItem item in input.Items)
            {
                BasketItem existingItem = combinedItems.FirstOrDefault(i => i.ProductId == item.ProductId);
                if (item.Quantity > 0)
                    if (existingItem != null)
                    {
                        existingItem.Quantity += item.Quantity;
                    }
                    else
                    {
                        combinedItems.Add(item);
                    }
            }

            Basket combined = new Basket(input.Id)
            {
                Items = combinedItems
            };
            storageProvider.Store(input.Id, combined);

            return combined;
        }

        public void Override(Basket input)
        {
            List<BasketItem> items = new List<BasketItem>();

            foreach (BasketItem item in input.Items)
            {
                BasketItem existingItem = items.FirstOrDefault(i => i.ProductId == item.ProductId);
                if (item.Quantity > 0)
                    if (existingItem != null)
                    {
                        existingItem.Quantity += item.Quantity;
                    }
                    else
                    {
                        items.Add(item);
                    }
            }

            Basket basketToStore = new Basket(input.Id)
            {
                Items = items
            };

            storageProvider.Store(basketToStore.Id, basketToStore);
        }

        public Basket RemoveItems(Basket input)
        {
            Basket existing = storageProvider.Get(input.Id);
            List<BasketItem> combinedItems = new List<BasketItem>();

            if (existing != null)
            {
                combinedItems.AddRange(existing.Items
                    .Where(i => !input.Items.ToList().Exists(b => b.ProductId == i.ProductId)));
            }
            Basket subtracted = new Basket(input.Id)
            {
                Items = combinedItems
            };
            storageProvider.Store(input.Id, subtracted);

            return subtracted;
        }

        public void Clear(Guid id)
        {
            Basket existing = storageProvider.Get(id);

            if (existing != null)
            {
                existing.Items = new List<BasketItem>();
            }

            storageProvider.Store(id, existing);
        }
    }
}
