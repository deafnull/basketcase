﻿namespace BasketCase.Core
{
    public struct Consts
    {
        public struct Config
        {
            public const string JwtIssuer = "Jwt:Issuer";
            public const string JwtAudience = "Jwt:Audience";
            public const string JwtSecretKey = "Jwt:SecretKey";
        }

        public struct ClaimTypes
        {
            public const string ApiKey = nameof(ApiKey);
        }
    }
}
