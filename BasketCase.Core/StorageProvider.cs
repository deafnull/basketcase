﻿using System.Collections.Concurrent;

namespace BasketCase.Core
{
    public interface IStorageProvider<TKey, TValue>
    {
        void Store(TKey key, TValue value);
        TValue Get(TKey key);
        void Remove(TKey key);
    }

    public class DictionaryStorage<TKey, TValue> : IStorageProvider<TKey, TValue>
    {
        private ConcurrentDictionary<TKey, TValue> dictionary = new ConcurrentDictionary<TKey, TValue>();

        public TValue Get(TKey key)
        {
            TValue val = default(TValue);
            dictionary.TryGetValue(key, out val);
            return val;
        }

        public void Store(TKey key, TValue value)
        {
            dictionary[key] = value;
        }

        public void Remove(TKey key)
        {
            TValue val = default(TValue);
            dictionary.TryRemove(key, out val);
        }
    }
}
