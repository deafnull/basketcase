﻿using BasketCase.Contracts;
using BasketCase.Contracts.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BasketCase.Client.Routes
{
    public class BasketApiRouteMap : IDisposable
    {
        private readonly HttpClient httpClient;

        public BasketApiRouteMap(HttpClient httpClient, string token)
        {
            this.httpClient = httpClient;
            httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {token}");
        }

        public async Task<HttpResponseMessage> Create()
        {
            string url = $"{ControllerNames.Basket}/{nameof(IBasketApi.Create)}"; 
            return await httpClient.GetAsync(url);
        }

        public async Task<HttpResponseMessage> Get(Guid basketId)
        {
            string url = $"{ControllerNames.Basket}/{nameof(IBasketApi.Get)}/{basketId}";
            return await httpClient.GetAsync(url);
        }

        public async Task<HttpResponseMessage> Put(Basket basket)
        {
            string url = $"{ControllerNames.Basket}/{nameof(IBasketApi.Put)}/{basket.Id}";
            string serialized = JsonConvert.SerializeObject(basket);
            HttpContent httpContent = new StringContent(serialized, Encoding.UTF8, "application/json");            
            return await httpClient.PutAsync(url, content: httpContent);
        }

        public async Task<HttpResponseMessage> Override(Basket basket)
        {
            string url = $"{ControllerNames.Basket}/{nameof(IBasketApi.Override)}/{basket.Id}";
            string serialized = JsonConvert.SerializeObject(basket);
            HttpContent httpContent = new StringContent(serialized, Encoding.UTF8, "application/json");
            return await httpClient.PostAsync(url, content: httpContent);
        }

        public async Task<HttpResponseMessage> RemoveItems(Basket basket)
        {
            string url = $"{ControllerNames.Basket}/{nameof(IBasketApi.RemoveItems)}/{basket.Id}";
            string serialized = JsonConvert.SerializeObject(basket);
            HttpContent httpContent = new StringContent(serialized, Encoding.UTF8, "application/json");
            return await httpClient.PutAsync(url, content: httpContent);
        }

        public async Task<HttpResponseMessage> Clear(Guid basketId)
        {
            string url = $"{ControllerNames.Basket}/{nameof(IBasketApi.Clear)}/{basketId}";
            return await httpClient.DeleteAsync(url);
        }

        public void Dispose()
        {
            httpClient?.Dispose();
        }
    }
}
