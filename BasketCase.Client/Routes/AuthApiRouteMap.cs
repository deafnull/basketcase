﻿using BasketCase.Contracts;
using BasketCase.Contracts.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BasketCase.Client.Routes
{
    public class AuthApiRouteMap : IDisposable
    {
        private readonly HttpClient httpClient;

        public AuthApiRouteMap(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }

        public async Task<HttpResponseMessage> RequestToken(TokenRequest request)
        {
            string url = $"{ControllerNames.Auth}/{nameof(IAuthApi.RequestToken)}";

            string serialized = JsonConvert.SerializeObject(request);

            HttpContent httpContent = new StringContent(serialized, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await httpClient.PostAsync(url, httpContent);
            
            return response;
        }

        public void Dispose()
        {
            httpClient?.Dispose();
        }
    }
}
