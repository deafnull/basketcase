﻿using BasketCase.Client.Routes;
using BasketCase.Contracts;
using BasketCase.Contracts.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace BasketCase.Client
{
    public class BasketApiClient : IBasketApi
    {
        private readonly BasketApiRouteMap router;

        public BasketApiClient(Uri baseAddress, string token)
        {
            router = new BasketApiRouteMap(new HttpClient
            {
                BaseAddress = baseAddress
            }, token);
        }

        public BasketApiClient(HttpClient httpClient, string token)
        {
            router = new BasketApiRouteMap(httpClient, token);
        }
                
        public async Task<Basket> Create()
        {
            HttpResponseMessage response = await router.Create();

            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            Basket basket = JsonConvert.DeserializeObject<Basket>(result);
            return basket;
        }

        public async Task<Basket> Get(Guid basketId)
        {
            HttpResponseMessage response = await router.Get(basketId);

            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            Basket basket = JsonConvert.DeserializeObject<Basket>(result);
            return basket;
        }

        public async Task<Basket> Put(Basket basket)
        {
            HttpResponseMessage response = await router.Put(basket);

            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Basket>(result);
        }

        public async Task Override(Basket basket)
        {
            HttpResponseMessage response = await router.Override(basket);

            response.EnsureSuccessStatusCode();
        }
        
        public async Task<Basket> RemoveItems(Basket basket)
        {
            HttpResponseMessage response = await router.RemoveItems(basket);

            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<Basket>(result);
        }

        public async Task Clear(Guid basketId)
        {
            HttpResponseMessage response = await router.Clear(basketId);

            response.EnsureSuccessStatusCode();
        }

        public void Dispose()
        {
            router.Dispose();
        }
    }
}
