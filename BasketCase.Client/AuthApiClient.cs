﻿using BasketCase.Client.Routes;
using BasketCase.Contracts;
using BasketCase.Contracts.Models;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace BasketCase.Client
{
    public class AuthApiClient : IAuthApi
    {
        private readonly AuthApiRouteMap router;

        public AuthApiClient(Uri baseAddress)
        {
            router = new AuthApiRouteMap(new HttpClient
            {
                BaseAddress = baseAddress
            });
        }

        public AuthApiClient(HttpClient httpClient)
        {
            router = new AuthApiRouteMap(httpClient);
        }

        public async Task<TokenResponse> RequestToken(string apiKey)
        {
            TokenRequest request = new TokenRequest
            {
                ApiKey = apiKey
            };

            HttpResponseMessage response = await router.RequestToken(request);

            response.EnsureSuccessStatusCode();

            string result = await response.Content.ReadAsStringAsync();

            TokenResponse token = JsonConvert.DeserializeObject<TokenResponse>(result);
            return token;
        }

        public void Dispose()
        {
            router.Dispose();
        }
    }
}
